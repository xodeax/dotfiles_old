# -*- coding: utf-8 -*-
import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Key, Screen, Group, Drag, Click, Match
from libqtile.lazy import lazy
from libqtile import layout, bar, widget, hook, utils
from typing import List  # noqa: F401
from libqtile.notify import notifier
from urllib.parse import quote, urlencode
from libqtile.widget import GenPollUrl, base

mod = "mod4"                                     # Sets mod key to SUPER/WINDOWS
myTerm = "alacritty"                             # My terminal of choice

keys = [
         ### The essentials
         Key([mod], "Return",
             lazy.spawn(myTerm),
             desc='Launches My Terminal'
             ),
         Key([mod, "shift"], "Return",
             lazy.spawn("dmenu_run -p 'Run: '"),
             desc='Dmenu Run Launcher'
             ),
         Key([mod], "Tab",
             lazy.next_layout(),
             desc='Toggle through layouts'
             ),
         Key([mod, "shift"], "c",
             lazy.window.kill(),
             desc='Kill active window'
             ),
         Key([mod, "shift"], "r",
             lazy.restart(),
             desc='Restart Qtile'
             ),
         Key([mod, "shift"], "q",
             lazy.shutdown(),
             desc='Shutdown Qtile'
             ),
         Key(["control", "shift"], "e",
             lazy.spawn("emacsclient -c -a emacs"),
             desc='Doom Emacs'
             ),
         ### Switch focus to specific monitor (out of three)
         #Key([mod], "w",
         #    lazy.to_screen(0),
         #    desc='Keyboard focus to monitor 1'
         #    ),
         #Key([mod], "e",
         #    lazy.to_screen(1),
         #    desc='Keyboard focus to monitor 2'
         #    ),
         #Key([mod], "r",
         #    lazy.to_screen(2),
         #    desc='Keyboard focus to monitor 3'
         #    ),
         ### Switch focus of monitors
         #Key([mod], "period",
         #    lazy.next_screen(),
         #    desc='Move focus to next monitor'
         #    ),
         #Key([mod], "comma",
         #    lazy.prev_screen(),
         #    desc='Move focus to prev monitor'
         #    ),
         ### Treetab controls
         #Key([mod, "control"], "k",
         #    lazy.layout.section_up(),
         #    desc='Move up a section in treetab'
         #    ),
         #Key([mod, "control"], "j",
         #    lazy.layout.section_down(),
         #    desc='Move down a section in treetab'
         #    ),
         ### Window controls
         Key([mod], "k",
             lazy.layout.down(),
             desc='Move focus down in current stack pane'
             ),
         Key([mod], "j",
             lazy.layout.up(),
             desc='Move focus up in current stack pane'
             ),
         Key([mod, "shift"], "k",
             lazy.layout.shuffle_down(),
             desc='Move windows down in current stack'
             ),
         Key([mod, "shift"], "j",
             lazy.layout.shuffle_up(),
             desc='Move windows up in current stack'
             ),
         Key([mod], "l",
             lazy.layout.grow(),
             lazy.layout.increase_nmaster(),
             desc='Expand window (MonadTall), increase number in master pane (Tile)'
             ),
         Key([mod], "h",
             lazy.layout.shrink(),
             lazy.layout.decrease_nmaster(),
             desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
             ),
         Key([mod], "n",
             lazy.layout.normalize(),
             desc='normalize window size ratios'
             ),
         Key([mod], "m",
             lazy.layout.maximize(),
             desc='toggle window between minimum and maximum sizes'
             ),
         Key([mod, "shift"], "f",
             lazy.window.toggle_floating(),
             desc='toggle floating'
             ),
         Key([mod, "shift"], "m",
             lazy.window.toggle_fullscreen(),
             desc='toggle fullscreen'
             ),
         ### Stack controls
         Key([mod, "shift"], "space",
             lazy.layout.rotate(),
             lazy.layout.flip(),
             desc='Switch which side main pane occupies (XmonadTall)'
             ),
         Key([mod], "space",
             lazy.layout.next(),
             desc='Switch window focus to other pane(s) of stack'
             ),
         Key([mod, "control"], "Return",
             lazy.layout.toggle_split(),
             desc='Toggle between split and unsplit sides of stack'
             ),
         ### Dmenu scripts launched with ALT + CTRL + KEY
         Key(["mod1", "control"], "e",
             lazy.spawn("./.dmenu/dmenu-edit-configs.sh"),
             desc='Dmenu script for editing config files'
             ),
         Key(["mod1", "control"], "m",
             lazy.spawn("./.dmenu/dmenu-sysmon.sh"),
             desc='Dmenu system monitor script'
             ),
         Key(["mod1", "control"], "p",
             lazy.spawn("passmenu"),
             desc='Passmenu'
             ),
         Key(["mod1", "control"], "r",
             lazy.spawn("./.dmenu/dmenu-reddio.sh"),
             desc='Dmenu reddio script'
             ),
         Key(["mod1", "control"], "s",
             lazy.spawn("./.dmenu/dmenu-surfraw.sh"),
             desc='Dmenu surfraw script'
             ),
         Key(["mod1", "control"], "t",
             lazy.spawn("./.dmenu/dmenu-trading.sh"),
             desc='Dmenu trading programs script'
             ),
         Key(["mod1", "control"], "i",
             lazy.spawn("./.dmenu/dmenu-scrot.sh"),
             desc='Dmenu scrot script'
             ),
         ### My applications launched with SUPER + ALT + KEY
         Key([mod, "mod1"], "b",
             lazy.spawn("brave"),
             desc='lynx browser'
             ),
         Key([mod, "mod1"], "l",
             lazy.spawn(myTerm+" -e lynx gopher://distro.tube"),
             desc='lynx browser'
             ),
         Key([mod, "mod1"], "n",
             lazy.spawn(myTerm+" -e newsboat"),
             desc='newsboat'
             ),
         Key([mod, "mod1"], "r",
             lazy.spawn(myTerm+" -e rtv"),
             desc='reddit terminal viewer'
             ),
         Key([mod, "mod1"], "e",
             lazy.spawn(myTerm+" -e neomutt"),
             desc='neomutt'
             ),
         Key([mod, "mod1"], "m",
             lazy.spawn(myTerm+" -e sh ./scripts/toot.sh"),
             desc='toot mastodon cli'
             ),
         Key([mod, "mod1"], "t",
             lazy.spawn(myTerm+" -e sh ./scripts/tig-script.sh"),
             desc='tig'
             ),
         Key([mod, "mod1"], "f",
             lazy.spawn(myTerm+" -e sh ./.config/vifm/scripts/vifmrun"),
             desc='vifm'
             ),
         Key([mod, "mod1"], "j",
             lazy.spawn(myTerm+" -e joplin"),
             desc='joplin'
             ),
         Key([mod, "mod1"], "c",
             lazy.spawn(myTerm+" -e cmus"),
             desc='cmus'
             ),
         Key([mod, "mod1"], "i",
             lazy.spawn(myTerm+" -e irssi"),
             desc='irssi'
             ),
         Key([mod, "mod1"], "y",
             lazy.spawn(myTerm+" -e youtube-viewer"),
             desc='youtube-viewer'
             ),
         Key([mod, "mod1"], "a",
             lazy.spawn(myTerm+" -e ncpamixer"),
             desc='ncpamixer'
             ),
]

#DT-------------------------------------
#group_names = [("WWW", {'layout': 'monadtall'}),
#               ("DEV", {'layout': 'monadtall'}),
#               ("SYS", {'layout': 'monadtall'}),
#               ("DOC", {'layout': 'monadtall'}),
#               ("VBOX", {'layout': 'monadtall'}),
#               ("CHAT", {'layout': 'monadtall'}),
#               ("MUS", {'layout': 'monadtall'}),
#               ("VID", {'layout': 'monadtall'}),
#               ("GFX", {'layout': 'floating'})]
#
#groups = [Group(name, **kwargs) for name, kwargs in group_names]
#
#for i, (name, kwargs) in enumerate(group_names, 1):
#    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
#    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group
#FIN DT------------------------------------------------

groups = []

# FOR QWERTY KEYBOARDS
#group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0",]

# FOR AZERTY KEYBOARDS
group_names = ["ampersand", "eacute", "quotedbl", "apostrophe", "parenleft", "minus",]

#group_names = ["ampersand", "eacute", "quotedbl", "apostrophe", "parenleft", "section", "egrave", "exclam", "ccedilla", "agrave",]

#group_labels = ["1 ", "2 ", "3 ", "4 ", "5 ", "6 ", "7 ", "8 ", "9 ", "0",]
#group_labels = ["", "", "", "", "", "", "", "", "", "",]
group_labels = ["ᛉ", "ᚾ", "ᛟ", "ᛒ", "ᛣ", "ᛗ",]

group_layouts = ["monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall",]
#group_layouts = ["monadtall", "verticaltile",]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))

for i in groups:
    keys.extend([

#CHANGE WORKSPACES
        Key([mod], i.name, lazy.group[i.name].toscreen()),
        #Key([mod], "Tab", lazy.screen.next_group()),
        Key(["mod1"], "Tab", lazy.screen.next_group()),
        Key(["mod1", "shift"], "Tab", lazy.screen.prev_group()),

# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND STAY ON WORKSPACE
        #Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND FOLLOW MOVED WINDOW TO WORKSPACE
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name) , lazy.group[i.name].toscreen()),
    ])



layout_theme = {"border_width": 2,
                "margin": 8,
                "border_focus": "e1acff",
                "border_normal": "1D2330"
                }

layouts = [
    #layout.MonadWide(**layout_theme),
    #layout.Bsp(**layout_theme),
    #layout.Stack(stacks=2, **layout_theme),
    #layout.Columns(**layout_theme),
    #layout.RatioTile(**layout_theme),
    #layout.Matrix(**layout_theme),
    #layout.Zoomy(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.VerticalTile(**layout_theme),
    layout.Max(**layout_theme),
    #layout.Tile(shift_windows=True, **layout_theme),
    #layout.Stack(num_stacks=2),
    #layout.TreeTab(
    #     font = "Ubuntu Mono Nerd Font",
    #     fontsize = 11,
    #     sections = ["FIRST", "SECOND"],
    #     section_fontsize = 11,
    #     bg_color = "141414",
    #     active_bg = "90C435",
    #     active_fg = "000000",
    #     inactive_bg = "384323",
    #     inactive_fg = "a0a0a0",
    #     padding_y = 5,
    #     section_top = 10,
    #     panel_width = 320
    #     ),
    layout.Floating(**layout_theme)
]

colors = [["#292d3e", "#292d3e"], # panel background
          ["#434758", "#434758"], # background for current screen tab
          ["#ffffff", "#ffffff"], # Monofont color for group names
          ["#ff5555", "#ff5555"], # border line color for current tab
          ["#8d62a9", "#8d62a9"], # border line color for other tab and odd widgets
          ["#668bd7", "#668bd7"], # color for the even widgets
          ["#e1acff", "#e1acff"]] # window name

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

##### DEFAULT WIDGET SETTINGS #####
widget_defaults = dict(
    font="Ubuntu Mono Nerd Font",
    fontsize = 14,
    padding = 2,
    background=colors[2]
)
extension_defaults = widget_defaults.copy()

def init_widgets_list():
    widgets_list = [
              widget.Sep(
                       linewidth = 0,
                       padding = 2,
                       foreground = colors[2],
                       background = colors[0]
                       ),
              widget.GroupBox(
                       font = "Ubuntu Mono Nerd Font",
                       fontsize = 14,
                       margin_y = 2,
                       margin_x = 0,
                       padding_y = 2,
                       padding_x = 2,
                       borderwidth = 3,
                       active = colors[2],
                       inactive = colors[5],
                       rounded = False,
                       #highlight_color = colors[4],
                       highlight_method = "block",
                       this_current_screen_border = colors[3],
                       this_screen_border = colors [4],
                       other_current_screen_border = colors[0],
                       other_screen_border = colors[0],
                       foreground = colors[2],
                       background = colors[0]
                       ),
              widget.TextBox(
                       text = '•',
                       background = colors[0],
                       foreground = colors[3],
                       padding = 4,
                       ),
              widget.CurrentLayout(
                       foreground = colors[2],
                       background = colors[0],
                       padding = 0
                       ),
              widget.Prompt(
                       prompt = prompt,
                       font = "Ubuntu Monoi Nerd Font",
                       padding = 0,
                       foreground = colors[3],
                       background = colors[0]
                       ),
              widget.TextBox(
                       text = '•',
                       background = colors[0],
                       foreground = colors[3],
                       padding = 4,
                       ),
              widget.WindowName(
                       foreground = colors[6],
                       background = colors[0],
                       padding = 0
                       ),
              widget.TextBox(
                       text = "µ:",
                       foreground = "e9c46a",
                       background = colors[0],
                       padding = 0,
                       fontsize = 14
                       ),
              widget.CPU(
                       foreground = "e9c46a",
                       background = colors[0],
                       format = '{load_percent}%',
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e htop')},
                       padding = 0,
                       update_interval = 2,
                       ),
              widget.TextBox(
                       text='•',
                       background = colors[0],
                       foreground = colors[3],
                       padding = 4,
                       ),
              widget.TextBox(
                       text = "m:",
                       foreground = "2a9d8f",
                       background = colors[0],
                       padding = 0,
                       fontsize = 14
                       ),
              widget.Memory(
                       foreground = "2a9d8f",
                       background = colors[0],
                       format = '{MemUsed}M',
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e htop')},
                       padding = 0,
                       update_interval = 2,
                       ),
              widget.TextBox(
                       text='•',
                       foreground = colors[3],
                       background = colors[0],
                       padding = 4,
                       ),
              widget.DF(
                       foreground = "f4a261",
                       background = colors[0],
                       format = '/:{r:.0f}%',
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e htop')},
                       padding = 0,
                       visible_on_warn = False,
                       ),
              widget.TextBox(
                       text='•',
                       background = colors[0],
                       foreground = colors[3],
                       padding = 4,
                       ),
              widget.GenPollUrl(
                       url='http://ifconfig.co/json',
                       #json=False,
                       #parse=lambda x: x.strip(),
                       parse=lambda x: x["asn_org"],
                       foreground = "55a630",
                       background = colors[0],
                       #padding = 2,
                       update_interval = 30,
                       ),
              widget.Net(
                       #interface = "wlp3s0",
                       format = ':{down},{up}',
                       foreground = "55a630",
                       background = colors[0],
                       padding = 2,
                       update_interval = 2,
                       ),
              widget.TextBox(
                       text = '•',
                       background = colors[0],
                       foreground = colors[3],
                       padding = 4,
                       ),
              #widget.Wttr(
              #         location = "'Vincennes': 'Vincennes'",
              #         foreground = "a8dadc",
              #         background = colors[0],
              #         mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn('wttr Vincennes')},
              #         ),
              widget.Clock(
                       foreground = "a8dadc",
                       background = colors[0],
                       format = " %a%d %b %H:%M",
                       padding = 0,
                       ),
              widget.TextBox(
                       text = '•',
                       background = colors[0],
                       foreground = colors[3],
                       padding = 4,
                       ),
              widget.Systray(
                       background = colors[0],
                       padding = 0
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 2,
                       foreground = colors[2],
                       background = colors[0]
                       ),
              ]
    return widgets_list

def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1                       # Slicing removes unwanted widgets on Monitors 1,3

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2                       # Monitor 2 will display all widgets in widgets_list

def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=1.0, size=20)),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), opacity=1.0, size=20))]

if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

#floating_layout = layout.Floating(float_rules=[
#    ,*layout.Floating.default_float_rules,
    #use xprop
#    Match(title='Confirmation'),
#    Match(wmclass='Qalculate!'),
#])
auto_fullscreen = True
focus_on_window_activation = "smart"

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
